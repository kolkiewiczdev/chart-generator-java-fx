package util.helper;

/**
 * File Helper class
 *
 * @author KOlkiewicz
 */
public class FileHelper {

    /**
     * Returns file extension based on passed as argument full file name.
     *
     * @param fileName Full file name (with extension).
     * @return File Extension
     */
    public static String getFileExtension(String fileName){
        String extension = "";

        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i+1);
        }

        return extension;
    }
}
