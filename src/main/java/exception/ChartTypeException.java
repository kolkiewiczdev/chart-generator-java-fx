package exception;

/**
 * Exception when Chart Type is invalid.
 *
 * @author KOlkiewicz
 */
public class ChartTypeException extends Exception {

    public ChartTypeException(String message){
        super(message);
    }
}
