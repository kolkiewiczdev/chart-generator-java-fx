package exception;

/**
 * Exception when ChartData is invalid.
 *
 * @author KOlkiewicz
 */
public class ChartDataException extends Exception {
    public ChartDataException(String message){
        super(message);
    }
}
