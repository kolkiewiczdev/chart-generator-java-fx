package exception;

/**
 * Exception when File Extension is invalid.
 *
 * @author KOlkiewicz
 */
public class FileExtensionException extends Exception {
    public FileExtensionException(String message){
        super(message);
    }
}
