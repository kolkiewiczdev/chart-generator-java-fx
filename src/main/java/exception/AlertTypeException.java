package exception;

/**
 * Exception when Alert type is invalid.
 *
 * @author KOlkiewicz
 */
public class AlertTypeException extends Exception {

    public AlertTypeException(String message){
        super(message);
    }
}
